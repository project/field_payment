Payment field
-------------

The field_payment field automatically creates a payment entity when the
entityform type to which the field is attached is submitted. These payments are
set up using the Payment module (https://www.drupal.org/project/payment).

This project was built because the paymentreference module did not meet the
requirements for an implementation of the Payment module in an entityform type.