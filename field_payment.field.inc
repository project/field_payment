<?php

/**
 * @file
 */

/**
 *
 */
function field_payment_field_info() {
  return array(
    'field_payment' => array(
      'label' => t('Payment'),
      'description' => t('Payment'),
      'default_widget' => 'field_payment_field_payment_form',
      'default_formatter' => 'field_payment_default',
      'property_type' => 'field_payment',
      'property_callbacks' => array('field_payment_property_info_callback'),
      'instance_settings' => array(
        'amount' => 0,
        'currency_code' => 'XXX',
        'description' => '',
        'method' => 0,
      ),
    ),
  );
}

/**
 * Checks if the given field is empty.
 */
function field_payment_field_is_empty($item, $field) {
  if ($field['type'] !== 'field_payment') {
    return FALSE;
  }

  if (empty($item['pid']) === TRUE) {
    return TRUE;
  }
  return FALSE;
}

/**
 *
 */
function field_payment_field_widget_info() {
  return array(
    'field_payment_field_payment_form' => array(
      'label' => t('Payment'),
      'field types' => array('field_payment'),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value' => FIELD_BEHAVIOR_DEFAULT,
      ),
    ),
  );
}

/**
 *
 */
function field_payment_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $elements) {
  if ($instance['widget']['type'] !== 'field_payment_field_payment_form') {
    return NULL;
  }

  $element = array();

  if (isset($items[$delta]['pid']) && !empty($items[$delta]['pid'])) {
    $element['pid'] = array(
      '#type' => 'markup',
      '#markup' => l('Betaling ' . $items[$delta]['pid'], 'payment/' . $items[$delta]['pid']),
      '#title' => $elements['#title'],
      '#field_display_label' => $instance['display']['default']['label'],
      '#attributes' => array(
        'class' => array('field_payment'),
      ),
    );

    $payment = entity_load('payment', array($items[$delta]['pid']));
    $payment = isset($payment[$items[$delta]['pid']]) ? $payment[$items[$delta]['pid']] : FALSE;

    if ($payment) {
      $status_items = payment_status_items($payment);
      $element['pid']['#markup'] .= render($status_items);
    }

  }

  return $element;
}

/**
 *
 */
function field_payment_field_formatter_info() {
  return array(
    'field_payment_default' => array(
      'label' => t('Default'),
      'field types' => array('field_payment'),
    ),
  );
}

/**
 *
 */
function field_payment_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'field_payment_default':
      foreach ($items as $delta => $item) {
        $element[$delta]['#markup'] = theme('field_payment_formatter_default', $item);
      }
      break;
  }

  return $element;
}

/**
 *
 */
function field_payment_theme($existing, $type, $theme, $path) {
  return array(
    'field_payment_formatter_default' => array(
      'variables' => array('item' => NULL),
    ),
  );
}

/**
 *
 */
function theme_field_payment_formatter_default($item) {

  $payment = entity_load('payment', array($item['pid']));
  $payment = isset($payment[$item['pid']]) ? $payment[$item['pid']] : FALSE;

  $output = l('Betaling ' . $item['pid'], 'payment/' . $item['pid']);

  if ($payment) {
    $status_items = payment_status_items($payment);
    $output .= render($status_items);
  }

  return $output;
}

/**
 * Gets the property just as it is set in the data.
 */
function field_payment_property_get($data, array $options, $name, $type, $info) {

  $payment = entity_load('payment', array($data['pid']));

  if (isset($payment[$data['pid']])) {
    return $payment[$data['pid']];
  }

  return NULL;
}

/**
 *
 */
function field_payment_property_info_callback(&$info, $entity_type, $field, $instance, $field_type) {
  $property = &$info[$entity_type]['bundles'][$instance['bundle']]['properties'][$field['field_name']];

  $property['getter callback'] = 'entity_metadata_field_verbatim_get';
  $property['setter callback'] = 'entity_metadata_field_verbatim_set';
  unset($property['query callback']);

  $property['property info']['pid'] = array(
    'type' => 'entity',
    'label' => t('Payment'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $property['property info']['status'] = array(
    'type' => 'payment',
    'label' => t('Payment'),
    'getter callback' => 'field_payment_property_get',
  );
}

/**
 *
 */
function field_payment_field_instance_settings_form($field, $instance) {
  $form = array();
  $form['currency_code'] = array(
    '#type' => 'select',
    '#title' => t('Currency'),
    '#options' => payment_currency_options(),
    '#default_value' => $instance['settings']['currency_code'],
    '#required' => TRUE,
  );
  $form['amount'] = array(
    '#type' => 'payment_amount',
    '#title' => t('Amount'),
    '#default_value' => $instance['settings']['amount'],
    '#required' => TRUE,
    '#currency_code' => '',
  );
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Payment description'),
    '#default_value' => $instance['settings']['description'],
    '#required' => TRUE,
  );
  $form['method'] = array(
    '#type' => 'select',
    '#title' => t('Payment method'),
    '#options' => payment_method_options(),
    '#default_value' => $instance['settings']['method'],
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Implements hook_form_alter().
 */
function field_payment_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id !== 'field_ui_field_edit_form' || $form['#field']['type'] !== 'field_payment') {
    return;
  }

  // Not required.
  $form['instance']['required']['#default_value'] = 0;

  // Hide fields.
  hide($form['instance']['required']);
  hide($form['instance']['description']);
  hide($form['instance']['description']);
  hide($form['instance']['default_value_widget']);
}
